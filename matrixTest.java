package com.junitdemo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class matrixTest {
	private static MatrixMultiplication m1;
	@BeforeAll
	public static void beforeAllMethod()
	{
	m1=new MatrixMultiplication();
		System.out.println("It will be executed once before all");
	}
	@Test
	public void testMethod1()
	{   int a[][] = { {1, 3, 2},{3, 1, 1},{1, 2, 2} };
	    int b[][] = { {2, 1, 1},{1, 0, 1},{1, 3, 1} };
		Assertions.assertTrue(m1.getMatrix(a, b));
	}
	@Test
	public void testMethod2()
	{ int a[][] = { {1, 3},{3, 1, 1},{1, 2, 2} };
    int b[][] = { {2, 1, 1},{1, 0},{1, 3, 1} };
	Assertions.assertFalse(m1.getMatrix(a, b));
	}
	@AfterAll
	public static void afterAllMethod()
	{
		System.out.println("It will be executed once after all");
	}
	
}
